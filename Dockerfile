# Use v0.10.2 as this appears to be the last tag that uses Ubuntu 16.04
FROM phusion/baseimage:0.10.2

# Set correct environment variables.
ENV HOME /root

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

# Maintained by Manoj
MAINTAINER Manoj Govindan <egmanoj@gmail.com>

# Set the locale. Default locale causes some Perl regexes to fail.
# http://jaredmarkell.com/docker-and-locales/
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# Latest git
RUN add-apt-repository ppa:git-core/ppa -y \
    && apt-get update -y \
    && apt-get install -y \
        git \
        build-essential \
        software-properties-common

# nodejs
# https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - \
    && apt-get update -y \
    && apt-get install --no-install-recommends -y -q ca-certificates \
    && apt-get install -y nodejs

# yarn
# https://yarnpkg.com/lang/en/docs/install/
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && apt-get update -y \
    && apt-get install -y yarn

# Reason
RUN yarn global add bs-platform

# Pyret
RUN git clone https://github.com/brownplt/pyret-lang.git \
    && cd pyret-lang \
    && npm install \
    && make \
    && make test

# Add a non-root user with non-password sudo access.
RUN useradd -ms /bin/bash dockerdev \
    && echo "dockerdev:dockerdev" | chpasswd \
    && adduser dockerdev sudo \
    && echo "%sudo ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

# Let dockerdev own /home/dockerdev and everything under it.
RUN chown -R dockerdev:dockerdev $(cat /etc/passwd | grep dockerdev | cut -f6 -d:)

# Running scripts during container startup
# See: https://github.com/phusion/baseimage-docker#running_startup_scripts
RUN mkdir -p /etc/my_init.d

# Bootup script to make dockerdev's UID/GID match the host user's.
ADD boot.sh /etc/my_init.d/01_boot.sh
RUN chmod 755 /etc/my_init.d/01_boot.sh

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Switch home to /home/dockerdev
ENV HOME /home/dockerdev

# Switch to the user's dir
WORKDIR /home/dockerdev
